# k8s-aws-client

[![pipeline status](https://gitlab.com/ucloud-tech/public/k8s-aws-client/badges/main/pipeline.svg)](https://gitlab.com/ucloud-tech/public/k8s-aws-client/-/commits/main)

[![coverage report](https://gitlab.com/ucloud-tech/public/k8s-aws-client/badges/main/coverage.svg)](https://gitlab.com/ucloud-tech/public/k8s-aws-client/-/commits/main)

[![Latest Release](https://gitlab.com/ucloud-tech/public/k8s-aws-client/-/badges/release.svg)](https://gitlab.com/ucloud-tech/public/k8s-aws-client/-/releases)